package com.vbnhat.teststreamingapp;

import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    final String url = "http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/0640_vod.m3u8";
    //http://123.30.185.126/hls/vtv1.m3u8
    private static final int MESSAGE_HANDLE_SEEK_BAR = 1;
    final MediaPlayer mPlayer = new MediaPlayer();
    private SurfaceView sfvStream = null;
    private SurfaceHolder sfHolder;
    private static final String TAG = MainActivity.class.getCanonicalName();
    private SeekBar seekBar;
    private  Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seekBar = (SeekBar) findViewById(R.id.seekbar);
        sfvStream = (SurfaceView) findViewById(R.id.sfvStream);



        sfvStream.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                sfHolder = holder;
                mPlayer.setDisplay(sfHolder);
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });


        try {
            mPlayer.setDataSource(url);
            mPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mPlayer.start();
        mPlayer.setScreenOnWhilePlaying(true);


        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case MESSAGE_HANDLE_SEEK_BAR:
                        updateSeekbar();
                        break;
                }
            }
        };

        initSeekbar();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                   updateSeekbar(progress);
                    mPlayer.seekTo(progress);
                    Toast.makeText(MainActivity.this,"Seek to: "+progress/1000+ "/ "+ mPlayer.getDuration()/1000 +" (s)",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void updateSeekbar(){
        seekBar.setMax(mPlayer.getDuration());
        handler.removeMessages(MESSAGE_HANDLE_SEEK_BAR);
        int currentPosition  = seekBar.getProgress();
        seekBar.setProgress(currentPosition+1000);
        handler.sendEmptyMessageDelayed(MESSAGE_HANDLE_SEEK_BAR,1000);
    }

    public void updateSeekbar(int position){
        seekBar.setMax(mPlayer.getDuration());
        handler.removeMessages(MESSAGE_HANDLE_SEEK_BAR);
        seekBar.setProgress(position+1000);
        handler.sendEmptyMessageDelayed(MESSAGE_HANDLE_SEEK_BAR,1000);
    }


    public void initSeekbar(){
        seekBar.setProgress(0);
        seekBar.setMax(mPlayer.getDuration());
        Toast.makeText(MainActivity.this,"Length: "+mPlayer.getDuration(),Toast.LENGTH_SHORT).show();
        handler.sendEmptyMessageDelayed(MESSAGE_HANDLE_SEEK_BAR,1000);
    }



    @Override
    protected void onPause(){
        mPlayer.release();
        super.onPause();
    }
}
